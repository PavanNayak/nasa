package com.abvious.homeactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abvious.homeactivity.Pojo.Nasa;
import com.abvious.homeactivity.utils.OnSwipeTouchListener;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {

    LinearLayout linear;
    Typeface myFont,myFont1;
    ImageView img;
    TextView txtexplanation,txttitle,txtdate;
    ArrayList<Nasa> nasa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        myFont = Typeface.createFromAsset(getAssets(), "Aller_Bd.ttf");
        myFont1 = Typeface.createFromAsset(getAssets(), "Aller_Rg.ttf");

        linear=findViewById(R.id.linear);
        img=findViewById(R.id.img);

//        Picasso.with(DetailActivity.this).load(getIntent().getStringExtra("img")).into(img);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
         nasa = (ArrayList<Nasa>) args.getSerializable("datalist");

        final int pos= Integer.parseInt(getIntent().getStringExtra("pos"));
        txtdate=findViewById(R.id.txtdate);
        txtexplanation=findViewById(R.id.txtexplanation);
        txttitle=findViewById(R.id.txttitle);
        Glide.with(DetailActivity.this).load(nasa.get(pos).getHdurl()).into(img);
        txttitle.setText(nasa.get(pos).getTitle());
        txtdate.setText(nasa.get(pos).getDate());
        txtexplanation.setText(nasa.get(pos).getExplanation());

        txttitle.setTypeface(myFont1);
        txtexplanation.setTypeface(myFont1);


        linear.setOnTouchListener(new OnSwipeTouchListener(DetailActivity.this) {
            public void onSwipeTop() {

            }
            public void onSwipeRight() {

                Log.d("SwipedRight","right1");
                Toast.makeText(DetailActivity.this, String.valueOf(nasa.get(pos+1).getTitle()), Toast.LENGTH_SHORT).show();

                Glide.with(DetailActivity.this).load(nasa.get(pos+1).getHdurl()).into(img);
                txttitle.setText(nasa.get(pos+1).getTitle());
                txtdate.setText(nasa.get(pos+1).getDate());
                txtexplanation.setText(nasa.get(pos+1).getExplanation());

            }
            public void onSwipeLeft() {

                Glide.with(DetailActivity.this).load(nasa.get(pos-1).getHdurl()).into(img);
                txttitle.setText(nasa.get(pos-1).getTitle());
                txtdate.setText(nasa.get(pos-1).getDate());
                txtexplanation.setText(nasa.get(pos-1).getExplanation());
            }
            public void onSwipeBottom() {

            }

        });

    }
}
