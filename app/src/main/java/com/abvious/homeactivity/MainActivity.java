package com.abvious.homeactivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.abvious.homeactivity.Pojo.Nasa;
import com.abvious.homeactivity.adapters.NasaAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    String resultArray="";
    List<Nasa> data;
    NasaAdapter adapter;
    RecyclerView recycler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recycler=findViewById(R.id.recycler);
        //load JSON Array from asset file
        resultArray=loadJSONFromAsset();
        parseJSON(resultArray);



    }


    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public void parseJSON(String array){
        data=new ArrayList<>();
        try {
            JSONArray jsonArray=new JSONArray(array);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject=jsonArray.getJSONObject(i);
                Nasa nasa=new Nasa();

                if(jsonObject.has("copyright"))
                nasa.setCopyright(jsonObject.getString("copyright"));
                else
                    nasa.setCopyright("");
                nasa.setDate(jsonObject.getString("date"));
                nasa.setExplanation(jsonObject.getString("explanation"));
                nasa.setHdurl(jsonObject.getString("hdurl"));
                nasa.setMedia_type(jsonObject.getString("media_type"));
                nasa.setService_version(jsonObject.getString("service_version"));
                nasa.setTitle(jsonObject.getString("title"));
                nasa.setUrl(jsonObject.getString("url"));

                data.add(nasa);

            }


            Collections.sort(data, new Comparator<Nasa>() {
                DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
                @Override
                public int compare(Nasa lhs, Nasa rhs) {
                    try {
                        return f.parse(lhs.getDate()).compareTo(f.parse(rhs.getDate()));
                    } catch (ParseException e) {
                        throw new IllegalArgumentException(e);
                    }
                }
            });


            adapter=new NasaAdapter(MainActivity.this,data);
            recycler.setFocusable(false);
            recycler.setAdapter(adapter);
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(MainActivity.this,2);
            recycler.setLayoutManager(layoutManager);
            adapter.notifyDataSetChanged();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
