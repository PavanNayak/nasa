package com.abvious.homeactivity.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.abvious.homeactivity.DetailActivity;
import com.abvious.homeactivity.Pojo.Nasa;
import com.abvious.homeactivity.R;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;


import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class NasaAdapter extends RecyclerView.Adapter<NasaAdapter.MyViewHolder> {
    private List<Nasa> dataList;
    private Context mContext;
    Typeface myFont,myFont1;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img;
        TextView txttitle;
        CardView cardView;
        public MyViewHolder(View view) {
            super(view);



            cardView=view.findViewById(R.id.cardView);
            img=view.findViewById(R.id.img);
            txttitle=view.findViewById(R.id.txttitle);

        }
    }


    public NasaAdapter(Context mContext, List<Nasa> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nasa_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Nasa data = dataList.get(position);


        myFont = Typeface.createFromAsset(mContext.getAssets(), "Aller_Bd.ttf");
        myFont1 = Typeface.createFromAsset(mContext.getAssets(), "Aller_Rg.ttf");



//        Picasso.with(mContext).load(data.getUrl()).into(holder.img);

        Glide.with(mContext).load(data.getUrl()).into(holder.img);

                holder.img.setScaleType(ImageView.ScaleType.FIT_XY);
            holder.txttitle.setText(data.getTitle());
            holder.txttitle.setTypeface(myFont1);

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i =new Intent(mContext, DetailActivity.class);
                    Bundle args = new Bundle();
                    i.putExtra("img",data.getHdurl());
                    i.putExtra("explanation",data.getExplanation());
                    i.putExtra("title",data.getTitle());
                    i.putExtra("date",data.getDate());
                    i.putExtra("pos",String.valueOf(position));
                    args.putSerializable("datalist", (Serializable)dataList);
                    i.putExtra("BUNDLE",args);
                    mContext.startActivity(i);

                }
            });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


}